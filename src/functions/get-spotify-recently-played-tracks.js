require("dotenv").config({ path: ".env.development" })
const axios = require("axios")
const querystring = require("querystring")

const refreshAccessToken = async () => {
  try {
    const response = await axios("https://accounts.spotify.com/api/token", {
      method: "POST",
      headers: {
        Authorization: `Basic ${new Buffer(
          process.env.SPOTIFY_CLIENT_ID +
            ":" +
            process.env.SPOTIFY_CLIENT_SECRET
        ).toString("base64")}`,
      },
      data: querystring.stringify({
        grant_type: "refresh_token",
        refresh_token: process.env.SPOTIFY_REFRESH_TOKEN,
      }),
    })
    const data = await response.data
    // console.log(data)
    return {
      // expiringDate: addSeconds(new Date(), data.expires_in),
      ...data,
    }
  } catch (error) {
    console.log(error)
  }
}

const getRecentlyPlayedTracks = async () => {
  const { access_token } = await refreshAccessToken()

  try {
    const response = await axios(
      "https://api.spotify.com/v1/me/player/recently-played?limit=5",
      {
        headers: { Authorization: `Bearer ${access_token}` },
      }
    )
    const data = await response.data
    return data.items.map(({ track }, index) => {
      const { id, name, preview_url, artists, album, external_urls } = track
      return {
        id: `${id}-${++index}`,
        name,
        artists: artists.map(({ id, name }) => ({ id, name })),
        preview_url,
        spotify_url: external_urls.spotify,
        cover_url: album.images[0].url,
      }
    })
  } catch (error) {
    console.log(error)
  }
}

export async function handler(event, context) {
  try {
    const recentlyPlayedTracks = await getRecentlyPlayedTracks()
    return {
      statusCode: 200,
      body: JSON.stringify(recentlyPlayedTracks),
    }
  } catch (error) {
    console.log(error)
    return {
      statusCode: 500,
      body: error,
    }
  }
}
