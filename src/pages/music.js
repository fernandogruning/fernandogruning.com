import React, { useEffect, useState } from "react"
import { useStaticQuery, graphql } from "gatsby"

import useSiteMetadata from "../queries/useSiteMetadata"

import Layout from "../components/Layout"
import SEO from "../components/SEO"
import Row, { RowItem } from "../components/Row"
import Artist from "../components/Artist"
import TrackItem from "../components/TrackItem"
import AlbumItem from "../components/AlbumItem"

import { introSection } from "../styles/global-styles"
import {
  bestArtistsSection,
  bestAlbumsSection,
  recentlyPlayedSection,
  artistsRowItemStyles,
} from "../styles/music-page"

export default () => {
  const { musicPage } = useSiteMetadata()
  const [recentlyPlayedTracks, setRecentlyPlayedTracks] = useState([])

  useEffect(() => {
    const fetchRecentlyPlayedTracks = async () => {
      const response = await fetch(
        "/.netlify/functions/get-spotify-recently-played-tracks"
      )
      const data = await response.json()
      setRecentlyPlayedTracks(data)
    }
    fetchRecentlyPlayedTracks()
  }, [])

  const { discogsList, allBestArtistsJson } = useStaticQuery(graphql`
    {
      discogsList {
        name
        items {
          id
          title
          artist
          image {
            childImageSharp {
              fluid(maxWidth: 240) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
      allBestArtistsJson(sort: { fields: order, order: ASC }) {
        nodes {
          id
          name
          order
          top
          image {
            childImageSharp {
              fluid(maxWidth: 420) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  `)

  return (
    <Layout>
      <SEO page={musicPage} />
      <section css={introSection}>
        <h1>Music</h1>
        <h3>My favorite artists, albums + my recently played tracks</h3>
      </section>

      <section css={bestArtistsSection}>
        <h2>Best Artists</h2>

        <Row>
          {allBestArtistsJson.nodes.map(({ id, ...artist }) => (
            <RowItem
              key={id}
              css={artistsRowItemStyles({
                isTop: artist.top,
                isTop3: artist.order && artist.order <= 3,
              })}
              className="row-item"
            >
              <Artist {...artist} />
            </RowItem>
          ))}
        </Row>
      </section>

      <section css={bestAlbumsSection}>
        <h2>Best Albums</h2>
        <Row>
          {discogsList &&
            discogsList.items.map(item => (
              <RowItem key={item.id} className="row-item">
                <AlbumItem {...item} />
              </RowItem>
            ))}
        </Row>
      </section>

      <section css={recentlyPlayedSection}>
        <h2>Recently Played</h2>
        <RecentTracks recentTracks={recentlyPlayedTracks} />
      </section>
    </Layout>
  )
}

// Move it to its own file (possibly)
const RecentTracks = ({ recentTracks }) => {
  // save the playingTrack id here
  const [playingTrack, setPlayingTrack] = useState(null)

  return recentTracks.length > 0 ? (
    <Row>
      {recentTracks.map(track => (
        <RowItem key={track.id} className="row-item">
          <TrackItem
            setPlayingTrack={setPlayingTrack}
            isPlaying={playingTrack === track.id}
            {...track}
          />
        </RowItem>
      ))}
    </Row>
  ) : (
    <p>Loading...</p>
  )
}
