import React from "react"

import useSiteMetadata from "../queries/useSiteMetadata"
import useProjectsData from "../queries/useProjectsData"

import Layout from "../components/Layout"
import SEO from "../components/SEO"
import ProjectsGrid from "../components/ProjectsGrid"

import { introSection } from "../styles/global-styles"
import { highlightedProjectsSection } from "../styles/projects-page"

export default () => {
  const { projectsPage } = useSiteMetadata()
  const { projects } = useProjectsData()
  return (
    <Layout>
      <SEO page={projectsPage} />
      <section css={introSection}>
        <h1>Projects</h1>
        <h3>Showcasing my latest projects</h3>
      </section>

      <section css={highlightedProjectsSection}>
        <div className="container">
          <ProjectsGrid projects={projects} />
        </div>
      </section>
    </Layout>
  )
}
