import React, { useState } from "react"
import { Formik, Form } from "formik"
import * as Yup from "yup"

import encode from "../utils/encode"

import useSiteMetadata from "../queries/useSiteMetadata"

import Layout from "../components/Layout"
import SEO from "../components/SEO"
import Row, { RowItem } from "../components/Row"
import FormInput from "../components/FormInput"
import FormTextarea from "../components/FormTextarea"

import GitHubIcon from "../components/svgs/GithubIcon"
import BitbucketIcon from "../components/svgs/BitbucketIcon"
import GitlabIcon from "../components/svgs/GitlabIcon"
import InstagramIcon from "../components/svgs/InstagramIcon"
import SpotifyIcon from "../components/svgs/SpotifyIcon"

import {
  wrapperStyles,
  introSection,
  contactFormSection,
  contactFormStyles,
  buttonStyles,
  contactLinksSection,
  contactLinkStyles,
} from "../styles/contact-page"

const ContactLink = ({ children, ...props }) => (
  <a
    css={contactLinkStyles}
    {...props}
    target="_blank"
    rel="noopener noreferrer"
  >
    {children}
  </a>
)

export default () => {
  const { contactPage } = useSiteMetadata()
  const [isSubmitted, setIsSubmitted] = useState(false)
  const [submissionError, setSubmissionError] = useState("")

  const handleSubmit = async (values, { resetForm }) => {
    setIsSubmitted(false)
    try {
      const response = await fetch("/", {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: encode({
          "form-name": "contact",
          ...values,
        }),
      })
      if (response) {
        console.log("Form submitted.")
        setIsSubmitted(true)
        resetForm()
      }
    } catch (error) {
      console.dir(error)
      setSubmissionError(error.message)
    }
  }

  return (
    <Layout>
      <SEO page={contactPage} />
      <div css={wrapperStyles}>
        <section css={introSection}>
          <h1>Contact Me</h1>
          <h3>Let's talk!</h3>
        </section>

        <section css={contactLinksSection}>
          <h3>
            I'm <span>@fernandogruning</span> everywhere!
          </h3>
          <ContactLink href="https://github.com/fernandogruning">
            <GitHubIcon className="icon" /> GitHub
          </ContactLink>
          <ContactLink href="https://bitbucket.org/fernandogruning">
            <BitbucketIcon className="icon" /> BitBucket
          </ContactLink>
          <ContactLink href="https://gitlab.com/fernandogruning">
            <GitlabIcon className="icon" /> GitLab
          </ContactLink>
          <ContactLink href="https://instagram.com/fernandogruning">
            <InstagramIcon className="icon" /> Instagram
          </ContactLink>
          <ContactLink href="https://open.spotify.com/user/fernandogruning">
            <SpotifyIcon className="icon" /> Spotify
          </ContactLink>
        </section>

        <section css={contactFormSection}>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            {formik => (
              <Form
                css={contactFormStyles}
                data-netlify="true"
                data-netlify-honeypot="bot-field"
                name="contact"
              >
                {/* The `form-name` hidden field is required to support form submissions without JavaScript */}
                <input type="hidden" name="form-name" value="contact" />
                <p hidden>
                  <label>
                    Don’t fill this out: <input name="bot-field" />
                  </label>
                </p>
                <Row>
                  <RowItem className="row-item half-width">
                    <FormInput label="Name" id="name" name="name" type="text" />
                  </RowItem>
                  <RowItem className="row-item half-width">
                    <FormInput
                      label="Email Address"
                      id="email"
                      name="email"
                      type="email"
                    />
                  </RowItem>
                  <RowItem className="row-item full-width">
                    <FormTextarea
                      label="Message"
                      id="message"
                      name="message"
                      rows="6"
                    />
                  </RowItem>
                  <RowItem>
                    <button
                      type="submit"
                      css={buttonStyles}
                      disabled={
                        formik.values === formik.initialValues ||
                        !formik.isValid ||
                        formik.isSubmitting
                      }
                    >
                      Send
                    </button>
                    {isSubmitted && (
                      <span className="alert alert-success">
                        Your message was sent!
                      </span>
                    )}
                    {submissionError && (
                      <span className="alert alert-failure">
                        Failed to send your message
                      </span>
                    )}
                  </RowItem>
                </Row>
              </Form>
            )}
          </Formik>
        </section>
      </div>
    </Layout>
  )
}

const initialValues = { name: "", email: "", message: "" }

const validationSchema = Yup.object({
  name: Yup.string().required("Required"),
  email: Yup.string()
    .email("Invalid email address")
    .required("Required"),
  message: Yup.string().required("required"),
})
