import React from "react"
import { Link } from "gatsby"

import useTopProjectsData from "../queries/useTopProjectsData"

import Layout from "../components/Layout"
import SEO from "../components/SEO"

import ReactLogo from "../components/svgs/ReactLogo"
import NodeLogo from "../components/svgs/NodeLogo"
import GatsbyLogo from "../components/svgs/GatsbyLogo"
import SassLogo from "../components/svgs/SassLogo"
import GraphQLLogo from "../components/svgs/GraphQLLogo"

import {
  introSection,
  aboutMeSection,
  highlightedProjectsSection,
  technologiesSection,
  contactSection,
} from "../styles/index-page"
import ProjectsGrid from "../components/ProjectsGrid"

export default () => {
  const { projects } = useTopProjectsData()

  return (
    <Layout>
      <SEO />
      <section css={introSection}>
        <h1>
          Hi,
          <br />
          I'm Fernando Gruning
          <span>Front-End Web Developer</span>
        </h1>
      </section>

      <section css={aboutMeSection}>
        <h2>About me</h2>
        <p>
          I'm from sunny Santo Domingo, Dominican Republic
          <span
            role="img"
            aria-label="Dominican Republic"
            title="Dominican Republic"
          >
            🇩🇴
          </span>
          . I'm 22 years old and I have 3 years of working experience in Web
          Development in an agency environment. Previously at{" "}
          <a href="http://boxxrd.com" target="_blank" rel="noopener noreferrer">
            Boxx Digital Agency
          </a>{" "}
          as Lead Web Developer & before that at{" "}
          <a
            href="https://dixdigitalagency.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            DIX Digital Agency
          </a>{" "}
          as Web Developer. Currently{" "}
          <span className="underline">freelancing</span> &{" "}
          <span className="underline">open to remote offers</span>.
        </p>

        <p>
          I'm really passionate about <Link to="music">music</Link>,{" "}
          <Link to="films-tv">films & TV</Link> and I enjoy cooking, reading
          about tech, art & media. I'd love to live one day in NYC
          <span role="img" aria-label="NYC" title="NYC">
            🗽
          </span>{" "}
          and my favorite country would be Sweden
          <span role="img" aria-label="Sweden" title="Sweden">
            🇸🇪
          </span>
          .
        </p>

        <p>
          <Link to="contact">Let's talk</Link>
        </p>
      </section>

      <section css={technologiesSection}>
        <h2>My main tools</h2>
        <div className="technologies">
          <ReactLogo className="technology react-logo" />
          <NodeLogo className="technology node-logo" />
          <GatsbyLogo className="technology gatsby-logo" />
          <SassLogo className="technology sass-logo" />
          <GraphQLLogo className="technology graphql-logo" />
        </div>
      </section>

      <section css={highlightedProjectsSection}>
        <h2>
          Highlighted Projects <Link to="projects">View all</Link>
        </h2>

        <div className="highlighted-projects">
          <ProjectsGrid projects={projects} />
        </div>
      </section>

      <section css={contactSection}>
        <div>
          <h2>I'm currently freelancing & open to remote work </h2>
          <Link to="contact">Let's talk</Link>
        </div>
      </section>
    </Layout>
  )
}
