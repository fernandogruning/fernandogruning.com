import React from "react"
import { graphql } from "gatsby"

import useSiteMetadata from "../queries/useSiteMetadata"

import Layout from "../components/Layout"
import SEO from "../components/SEO"
import Row, { RowItem } from "../components/Row"
import FavoriteItem from "../components/FavoriteItem"

import { introSection } from "../styles/global-styles"
import {
  favoriteFilmsSection,
  favoriteTVShowsSection,
} from "../styles/films-tv-page"

export default ({ data }) => {
  const { filmsTvPage } = useSiteMetadata()

  return (
    <Layout>
      <SEO page={filmsTvPage} />
      <section css={introSection}>
        <h1>Films + TV</h1>
        <h3>The films & TV shows that made me wonder</h3>
      </section>

      <section css={favoriteFilmsSection}>
        <h2>Favorite Films</h2>
        <ItemRow items={data.favoriteMovies.items} />
      </section>

      <section css={favoriteTVShowsSection}>
        <h2>Favorite TV Shows</h2>
        <ItemRow items={data.favoriteTVShows.items} />
      </section>
    </Layout>
  )
}

const ItemRow = ({ items }) => (
  <Row>
    {items.map(item => (
      <RowItem key={item.id} className="row-item">
        <FavoriteItem {...item} />
      </RowItem>
    ))}
  </Row>
)

export const query = graphql`
  fragment tmdbListData on TMDBAccountLists {
    id
    name
    item_count
    items {
      name
      title
      media_type
      listItemId
      poster_path {
        childImageSharp {
          fluid(maxWidth: 576) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  }
  {
    favoriteMovies: tmdbAccountLists(name: { eq: "Favorite Movies" }) {
      ...tmdbListData
    }
    favoriteTVShows: tmdbAccountLists(name: { eq: "Favorite TV Shows" }) {
      ...tmdbListData
    }
  }
`
