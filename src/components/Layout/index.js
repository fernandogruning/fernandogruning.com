import React from "react"
import { css, Global } from "@emotion/core"

import mq from "../../utils/media-queries"

import { ThemeProvider } from "emotion-theming"
import Header from "../Header"
import Footer from "../Footer"

const theme = {
  headerFontFamily: `"CircularStd", "-apple-system", "BlinkMacSystemFont", "Segoe UI",
    "Roboto", "Helvetica Neue", "Arial", "Noto Sans", "sans-serif",
    "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";`,
  colors: {
    primary: "tomato",
    primaryDarker: "#ff3814",
  },
  styles: {
    transition: "all 0.225s ease",
  },
}

const Layout = ({ children }) => {
  return (
    <ThemeProvider theme={theme}>
      <Global
        styles={theme => css`
          body {
            background-color: #fbfbfb;
          }

          a {
            text-decoration: none;
            color: ${theme.colors.primary};
            transition: ${theme.styles.transition};

            &:hover {
              color: ${theme.colors.primaryDarker};
            }
          }
        `}
      />
      <div
        css={css`
          padding: 0 1rem;

          ${mq[1]} {
            padding: 0 2rem;
          }

          ${mq[4]} {
            max-width: 80rem;
            padding: 0;
            margin: 0 auto;
          }
        `}
      >
        <Header />
        <main>{children}</main>
        <Footer />
      </div>
    </ThemeProvider>
  )
}

export default Layout
