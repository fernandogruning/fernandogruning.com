import React from "react"

import styles from "./styles"

const MusicItem = ({ children, title, artist, className, ...props }) => {
  return (
    <div className={`music-item ${className}`} css={styles} {...props}>
      {children}
      <h4 className="item-title">{title}</h4>
      <h5>{artist}</h5>
    </div>
  )
}

export default MusicItem
