import { css } from "@emotion/core"

// prettier-ignore
export default  (css`
  display: block;
  width: 100%;
  text-align: center;

  .item-title {
    margin-bottom: 0;

    span {
      color: blue;
      cursor: pointer;
      font-size: 1rem;
      font-weight: 400;
    }
  }

  h5 {
    margin-bottom: 0;
    font-weight: 400;
    line-height: 1.1;
  }
`)
