import React from "react"
import PropTypes from "prop-types"
import Img from "gatsby-image"

import cardStyles from "./styles"

const ProjectCard = ({
  title,
  description,
  githubURL,
  gitlabURL,
  websiteURL,
  image,
}) => {
  return (
    <div css={cardStyles}>
      {websiteURL ? (
        <a
          href={websiteURL}
          className="card-image"
          target="_blank"
          rel="noopener noreferrer"
        >
          {image ? <Img fluid={image} /> : null}
        </a>
      ) : (
        <div className="card-image">{image ? <Img fluid={image} /> : null}</div>
      )}
      <div className="card-content">
        <h3 className="card-title">{title}</h3>
        <div className="description">
          <p>{description}</p>
        </div>

        <div className="link-group">
          {githubURL && (
            <a
              href={githubURL}
              target="_blank"
              rel="noopener noreferrer"
              className="link"
            >
              GitHub
            </a>
          )}
          {gitlabURL && (
            <a
              href={gitlabURL}
              target="_blank"
              rel="noopener noreferrer"
              className="link"
            >
              GitLab
            </a>
          )}
          {websiteURL && (
            <a
              href={websiteURL}
              target="_blank"
              rel="noopener noreferrer"
              className="link"
            >
              Visit
            </a>
          )}
        </div>
      </div>
    </div>
  )
}

ProjectCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  githubURL: PropTypes.string,
  gitlabURL: PropTypes.string,
  websiteURL: PropTypes.string,
  image: PropTypes.shape({
    aspectRatio: PropTypes.number,
    base64: PropTypes.string,
    sizes: PropTypes.string,
    src: PropTypes.string,
    srcSet: PropTypes.string,
  }),
}

export default ProjectCard
