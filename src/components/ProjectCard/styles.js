import { css } from "@emotion/core"

import mediaQueries from "../../utils/media-queries"

// prettier-ignore
export default (css`
  display: block;
  width: 100%;
  border-radius: 0.5rem;
  box-shadow: 0 4px 12px  rgba(0,0,0,0.2);

  ${mediaQueries[1]} {
    max-width: 576px;
  }

  .card-image {
    position: relative;
    display: block;
    width: 100%;
    border-top-left-radius: calc(0.5rem - 1px);
    border-top-right-radius: calc(0.5rem - 1px);
    overflow: hidden;
  }

  .card-content {
    padding: 1.5rem;
  }

  .card-title {
    margin-bottom: 1rem;
  }

  .description {
    min-height: 58px;
    margin-bottom: 1.5rem;

    p:last-of-type {
      margin-bottom: 0;
    }
  }

  .link-group {
    display: flex;
    justify-content: flex-end;

    .link {
      &:not(:last-child) {
        margin-right: 1.5rem;
      }
    }
  }
`)
