import { css } from "@emotion/core"

// prettier-ignore
export default (css`
  .cover-image {
    border-radius: 0.5rem;
    margin-bottom: 0.5rem;
    box-shadow: 0 4px 12px rgba(0,0,0, 0.2);
    overflow: hidden;
  }
`)
