import React from "react"
import Img from "gatsby-image"

import MusicItem from "../MusicItem"

import styles from "./styles"

const AlbumItem = ({ title, artist, image }) => {
  return (
    <MusicItem
      title={title}
      artist={artist}
      className="album-item"
      css={styles}
    >
      <div className="cover-image">
        <Img fluid={image.childImageSharp.fluid} />
      </div>
    </MusicItem>
  )
}

export default AlbumItem
