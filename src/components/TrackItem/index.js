import React, { useRef, useEffect } from "react"

import MusicItem from "../MusicItem"
import PlayIcon from "../svgs/PlayIcon"
import PauseIcon from "../svgs/PauseIcon"

import styles from "./styles"

const TrackItem = ({
  isPlaying,
  setPlayingTrack,
  id,
  name,
  cover_url,
  artists,
  preview_url,
}) => {
  const audioRef = useRef(null)

  // when this Track is clicked, pause or play it
  const toggleAudio = isPlaying => {
    if (isPlaying) {
      setPlayingTrack(null)
      audioRef.current.pause()
    } else {
      setPlayingTrack(id)
      audioRef.current.play()
    }
  }

  // This is the equivalent of didComponentUpdate
  // So when isPlaying prop changes run this hook
  // This is used when another track begins playing
  // to pause the playing track or play it if it gets
  // clicked
  useEffect(() => {
    !isPlaying ? audioRef.current.pause() : audioRef.current.play()
  }, [isPlaying])

  const joinedArtists = artists.reduce((completeStr, artist, index) => {
    if (index === 0) return artist.name
    return `${completeStr}, ${artist.name}`
  }, "")

  return (
    <MusicItem
      title={name}
      artist={joinedArtists}
      className="track-item"
      css={styles}
    >
      {preview_url && (
        <audio
          src={preview_url}
          ref={audioRef}
          onEnded={() => toggleAudio(isPlaying)}
        ></audio>
      )}
      <div className="cover-image" onClick={() => toggleAudio(isPlaying)}>
        {preview_url && (
          <React.Fragment>
            <PlayIcon className={!isPlaying ? "show" : null} />
            <PauseIcon className={isPlaying ? "show" : null} />
          </React.Fragment>
        )}
        <img src={cover_url} alt={`${name} - ${joinedArtists}`} />
      </div>
    </MusicItem>
  )
}

export default TrackItem
