import { css } from "@emotion/core"

// prettier-ignore
export default  (css`
  .cover-image {
    position: relative;
    width: 100%;
    height: 0;
    padding-top: 100%;
    border-radius: 0.5rem;
    margin-bottom: 0.5rem;
    overflow: hidden;
    box-shadow: 0 4px 12px rgba(0,0,0,0.2);

    img {
      position: absolute;
      top: 0;
      left: 0;
      display: block;
      width: 100%;
      height: 100%;
      object-fit: cover;
    }

    .feather {
      z-index: 9999;
      position: absolute;
      top: 50%;
      left: 50%;
      width: 4rem;
      color: #fff;
      transform: translate(-50%, -50%) scale(0.75);
      filter: drop-shadow(0 0 8px  rgba(0,0,0,0.6));
      opacity: 0;
      pointer-events: none;
      transition: all 0.225s ease;
      cursor: pointer;
      will-change: transform, opacity, color;

      &.show {
        opacity: 1;
        pointer-events: auto;
        transform: translate(-50%, -50%) scale(1);
      }
    }
  }
`)
