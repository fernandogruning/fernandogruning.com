import React from "react"
import { Helmet } from "react-helmet"
import useSiteMetadata from "../../queries/useSiteMetadata"

const SEO = ({ page }) => {
  const site = useSiteMetadata()

  const lang = page ? (page.lang ? page.lang : site.lang) : site.lang

  const title = page ? (page.title ? page.title : site.title) : site.title

  const description = page
    ? page.description
      ? page.description
      : site.description
    : site.description

  const url = page ? (page.url ? page.url : site.siteUrl) : site.siteUrl

  return (
    <Helmet>
      <title itemProp="name">{title}</title>

      <meta name="description" content={description} />
      <link rel="canonical" href={url} />
    </Helmet>
  )
}

export default SEO
