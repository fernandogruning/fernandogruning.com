import { css } from "@emotion/core"
import mq from "../../utils/media-queries"

// prettier-ignore
export default theme => (css`
  display: flex;
  align-items: center;
  padding: 1rem 0;
  font-family: ${theme.headerFontFamily};

  .desktop-nav {
    margin-left: auto;
    display: none;

    ${mq[1]} {
      display: flex;
    }

    a {
      padding: 0.5rem 1rem;
      color: currentColor;

      &.active {
        color: ${theme.colors.primaryDarker};
      }

      &:not(:last-child) {
        margin-right: 1rem;
      }
    }
  }

  .menu-toggler {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-left: auto;
    color: currentColor;

    .feather-menu {
      height: 2.5rem;
    }

    ${mq[1]} {
      display: none;
    }
  }

  .mobile-menu {
    z-index: 9999;
    position: absolute;
    top: 1rem;
    left: 1rem;
    display: flex;
    align-items: center;
    width: calc(100vw - 2rem);
    height: calc(100% - 2rem);
    background-color: #fff;
    padding: 2rem;
    border-radius: 0.5rem;
    box-shadow: 0px 12px 24px 0px rgba(0,0,0,0.2);
    opacity: 0;
    pointer-events: none;
    transform: scale(0.95);
    transform-origin: 100% 0;
    transition: ${theme.styles.transition};

    ${mq[1]} {
      display: none;
    }

    &.open {
      pointer-events: auto;
      opacity: 1;
      transform: scale(1);
    }

    nav {
      display: flex;
      flex-direction: column;

      a {
        color: currentColor;
        font-size: 2rem;
        margin-bottom: 1.25rem;
        line-height: 1.2;

        &.active {
          color: ${theme.colors.primaryDarker}
        }

        &:last-of-type {
          margin-bottom: 0;
        }
      }
    }

    .close {
      position: absolute;
      top: 1rem;
      right: 1rem;
      color: currentColor;

      .feather-x {
        height: 2.5rem;
      }
    }
  }
`)

// prettier-ignore
export const logoStyles = (css`
  display: flex;

  .fernandogruning-logo {
    width: 4rem;
  }
`)
