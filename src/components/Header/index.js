import React, { useState } from "react"
import { Link } from "gatsby"

import FernandoGruningLogo from "../svgs/FernandoGruningLogo"
import MobileMenuIcon from "../svgs/MobileMenuIcon"
import CloseIcon from "../svgs/CloseIcon"

import headerStyles, { logoStyles } from "./styles"

const links = [
  { url: "/projects", title: "Projects" },
  {
    url: "/music",
    title: (
      <React.Fragment>
        <span role="img" aria-label="Favorite" title="Favorite">
          ⭐️
        </span>
        Music
      </React.Fragment>
    ),
  },
  {
    url: "/films-tv",
    title: (
      <React.Fragment>
        <span role="img" aria-label="Favorite" title="Favorite">
          ⭐️
        </span>
        Films + TV
      </React.Fragment>
    ),
  },
  { url: "/contact", title: "Contact Me" },
]

const Logo = () => (
  <Link to="/" css={logoStyles}>
    <FernandoGruningLogo />
  </Link>
)

const Nav = ({ className, handleClick }) => (
  <nav className={className}>
    {links.map(link => (
      <Link
        key={link.url}
        to={link.url}
        onClick={handleClick}
        activeClassName="active"
      >
        {link.title}
      </Link>
    ))}
  </nav>
)

const Header = () => {
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false)

  return (
    <div>
      <header css={headerStyles}>
        <Logo />
        <Nav className="desktop-nav " />
        <div
          className="menu-toggler"
          onClick={() => setIsMobileMenuOpen(!isMobileMenuOpen)}
        >
          <MobileMenuIcon />
        </div>
        <div className={`mobile-menu ${isMobileMenuOpen ? "open" : ""}`}>
          <div
            className="close"
            onClick={() => setIsMobileMenuOpen(!isMobileMenuOpen)}
          >
            <CloseIcon />
          </div>
          <Nav handleClick={() => setIsMobileMenuOpen(!isMobileMenuOpen)} />
        </div>
      </header>
    </div>
  )
}

export default Header
