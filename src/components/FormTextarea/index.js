import React from "react"
import FormField from "../FormField"

const FormTextarea = props => {
  return (
    <FormField {...props}>
      {formFieldProps => <textarea {...formFieldProps} />}
    </FormField>
  )
}

export default FormTextarea
