import { css } from "@emotion/core"

import mq from "../../utils/media-queries"

// prettier-ignore
export default (css`
  display: grid;
  grid-gap: 1rem;
  align-items: stretch;

  ${mq[1]} {
    grid-template-columns: repeat(2, 1fr);
  }

  ${mq[2]} {
    grid-template-columns: repeat(3, 1fr);
  }
`)
