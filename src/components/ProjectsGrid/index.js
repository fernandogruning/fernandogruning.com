import React from "react"

import ProjectCard from "../ProjectCard"

import styles from "./styles"

const ProjectsGrid = ({ projects }) => {
  return (
    <div css={styles} className="project-grid">
      {projects.map(({ id, image, ...project }) => (
        <ProjectCard
          key={id}
          image={image ? image.childImageSharp.fluid : null}
          {...project}
        />
      ))}
    </div>
  )
}

export default ProjectsGrid
