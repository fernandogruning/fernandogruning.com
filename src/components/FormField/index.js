import React from "react"
import { useField } from "formik"

import styles from "./styles"

const FormField = ({ label, children, ...props }) => {
  const [field, meta] = useField(props)

  return (
    <div css={styles} className="form-field">
      <label htmlFor={props.id || props.name}>
        {label}{" "}
        {meta.touched && meta.error && (
          <span className="error">{meta.error}</span>
        )}
      </label>
      {children({ ...field, ...props })}
    </div>
  )
}

export default FormField
