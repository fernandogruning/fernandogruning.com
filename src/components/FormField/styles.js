import { css } from "@emotion/core"

// prettier-ignore
export default (css`
  display: flex;
  flex-direction: column;

  label {
    margin-bottom: 0.5rem;
    font-size: 18px;
    font-weight: 600;

    .error {
      font-size: 12px;
      font-weight: 400;
      color: red;
      text-transform: uppercase;
    }
  }

  input, textarea {
    max-width: 100%;
    min-height: fit-content;
    padding: 0.5rem 0.75rem;
    border: none;
    border-radius: 0.25rem;
    background: rgba(0,0,0,0.1);
    font-size: 18px;
    transition: background-color 0.225s ease;

    &:hover, &:focus {
      outline: none;
      background-color: rgba(0,0,0,0.175);
    }
  }
`)
