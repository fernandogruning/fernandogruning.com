import React from "react"

const Footer = () => {
  return (
    <footer>
      <small>
        &copy;{new Date().getFullYear()} Made with{" "}
        <span role="img" aria-label="love" title="love">
          ❤️
        </span>{" "}
        by Fernando Gruning. View code on{" "}
        <a
          href="https://bitbucket.org/fernandogruning/fernandogruning.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          BitBucket
        </a>
        .
      </small>
    </footer>
  )
}

export default Footer
