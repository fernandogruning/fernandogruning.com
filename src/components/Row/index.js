import React from "react"

import styles, { rowItemStyles } from "./styles"

export const RowItem = ({ children, ...props }) => (
  <div css={rowItemStyles} {...props}>
    {children}
  </div>
)

const Row = ({ children }) => {
  return (
    <div css={styles} className="row">
      {children}
    </div>
  )
}

export default Row
