import { css } from "@emotion/core"

// prettier-ignore
export default (css`
  display: flex;
  flex-wrap: wrap;
  margin-left: -0.5rem;
  margin-right: -0.5rem;
`)

// prettier-ignore
export const rowItemStyles = (css`
  flex: 0 0 100%;
  width: 100%;
  max-width: 100%;
  padding: 0 0.5rem;
`)
