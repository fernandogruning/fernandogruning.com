import React from "react"
import Img from "gatsby-image"

import styles, { topStyles, top3Styles } from "./styles"
import PropTypes from "prop-types"

const Artist = ({ name, top, order, image }) => {
  const allStyles = [styles]

  // if artist data has top prop, add topStyles styling
  if (top) allStyles.push(topStyles)

  // if artist data has order prop & is in the top 3, add top3Styles styling
  if (order && order <= 3) allStyles.push(top3Styles)

  return (
    <div css={allStyles}>
      <Img fluid={image.childImageSharp.fluid} className="artist-image" />
      {top ? (
        <h3 className="artist-name">{name}</h3>
      ) : (
        <h4 className="artist-name">{name}</h4>
      )}
    </div>
  )
}

Artist.propTypes = {
  artist: PropTypes.shape({
    name: PropTypes.string.isRequired,
    top: PropTypes.bool,
    order: PropTypes.number,
    image: PropTypes.shape({
      aspectRatio: PropTypes.number.isRequired,
      src: PropTypes.string.isRequired,
      srcSet: PropTypes.string.isRequired,
      sizes: PropTypes.string.isRequired,
      base64: PropTypes.string,
      tracedSVG: PropTypes.string,
      srcWebp: PropTypes.string,
      srcSetWebp: PropTypes.string,
      media: PropTypes.string,
    }),
  }),
}

export default Artist
