import { css } from "@emotion/core"

// prettier-ignore
const styles = (css`
  position: relative;
  width: 100%;
  height: 0;
  padding-top: 100%;
  border-radius: 0.5rem;
  box-shadow: 0 4px 12px rgba(0,0,0, 0.2);
  overflow: hidden;

  .artist-image {
    position: absolute !important; /* gatsby-image position fix */
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }

  .artist-name {
    position: absolute;
    bottom: 0;
    left: 0;
    padding: 1rem;
    margin-bottom: 0;
    color: #ffffff;
    text-shadow: 0 0 8px rgba(0,0,0, 0.6);
  }
`)

export default styles

//prettier-ignore
export const topStyles = (css`
  padding-top: calc(100% / (16/10));
`)

// prettier-ignore
export const top3Styles = (css`
  padding-top: calc(100% / (10/16));

  .artist-name {
    bottom: unset;
    left: unset;
    top: 0;
    right: 0;
    text-align: right;
  }
`)
