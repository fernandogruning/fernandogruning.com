import React from "react"

import FormField from "../FormField"

const FormInput = props => {
  return (
    <FormField {...props}>
      {formFieldProps => (
        <input type={props.type || "text"} {...formFieldProps} />
      )}
    </FormField>
  )
}

export default FormInput
