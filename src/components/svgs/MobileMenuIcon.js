import React from "react"

const MobileMenuIcon = props => (
  <svg
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth={3}
    strokeLinecap="round"
    strokeLinejoin="round"
    className="feather feather-menu"
    {...props}
  >
    <path d="M3 12L21 12" />
    <path d="M3 6L21 6" />
    <path d="M3 18L21 18" />
  </svg>
)

export default MobileMenuIcon
