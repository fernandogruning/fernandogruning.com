import React from "react"

const FernandoGruningLogo = props => (
  <svg viewBox="0 0 512 512" className="fernandogruning-logo" {...props}>
    <defs>
      <clipPath id="a">
        <path d="M0 0h512v512H0z" />
      </clipPath>
    </defs>
    <g data-name="custom \u2013 1">
      <g data-name="Group 1" fill="currentColor" clipPath="url(#a)">
        <circle
          data-name="Ellipse 1"
          cx={32}
          cy={32}
          r={32}
          transform="translate(32 224)"
        />
        <circle
          data-name="Ellipse 2"
          cx={32}
          cy={32}
          r={32}
          transform="translate(416 224)"
        />
        <path
          data-name="Path 1"
          d="M382.882 387.787v-137.5H250.969v44.716h83.47c-2.981 18.632-21.985 51.424-70.8 51.424-44.344 0-83.47-30.929-83.47-90.55 0-62.6 43.226-89.8 83.1-89.8 42.853 0 64.838 27.2 71.546 50.678l49.188-17.514c-13.042-40.617-51.424-81.234-120.734-81.234-71.546 0-135.266 52.542-135.266 137.875s60.736 137.495 131.906 137.495c39.872 0 65.956-17.886 77.881-35.773l3.354 30.183z"
        />
      </g>
    </g>
  </svg>
)

export default FernandoGruningLogo
