import React from "react"

const PauseIcon = ({ className, ...props }) => (
  <svg
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth={3}
    strokeLinecap="round"
    strokeLinejoin="round"
    className={`feather feather-pause ${className}`}
    {...props}
  >
    <path d="M6 4H10V20H6z" />
    <path d="M14 4H18V20H14z" />
  </svg>
)

export default PauseIcon
