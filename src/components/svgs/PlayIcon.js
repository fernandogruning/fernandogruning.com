import React from "react"

const PlayIcon = ({ className, ...props }) => (
  <svg
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth={3}
    strokeLinecap="round"
    strokeLinejoin="round"
    className={`feather feather-play ${className}`}
    {...props}
  >
    <path d="M5 3L19 12 5 21 5 3z" />
  </svg>
)

export default PlayIcon
