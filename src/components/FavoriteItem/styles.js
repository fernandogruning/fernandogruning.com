import { css } from "@emotion/core"

// prettier-ignore
export default (css`
  display: block;
  color: inherit;
  text-align: center;
  text-decoration: none;


  .item-poster {
    margin-bottom: 1rem;
    box-shadow: 0px 12px 24px 0px rgba(0,0,0,0.2)
  }

  .item-title {
    font-weight: 600;
    margin-bottom: 0;
  }
`)
