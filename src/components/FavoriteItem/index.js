import React from "react"
import Img from "gatsby-image"

import styles from "./styles"

const FavoriteItem = ({
  media_type,
  listItemId,
  poster_path,
  title,
  name,
  ...props
}) => {
  return (
    <a
      className="favorite-item"
      href={`https://tmdb.org/${media_type}/${listItemId}`}
      target="_blank"
      rel="noopener noreferrer"
      {...props}
      css={styles}
    >
      <Img fluid={poster_path.childImageSharp.fluid} className="item-poster" />
      <h4 className="item-title">{title || name}</h4>
    </a>
  )
}

export default FavoriteItem
