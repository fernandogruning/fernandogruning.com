import { css } from "@emotion/core"

import { sectionStyles, introSection as ogIntroSection } from "./global-styles"

import mq from "../utils/media-queries"

// prettier-ignore
const introSection = theme => (css`
  ${ogIntroSection}

  h1 {
    span {
      display: block;
      color: ${theme.colors.primary};
    }
  }
`)

// prettier-ignore
const aboutMeSection = theme => (css`
  ${sectionStyles}

  h2 {
    span {
      text-decoration: underline;
    }
  }

  p {
    font-size: 1.5rem;
    max-width: 80ch;
  }

  .underline {
    text-decoration: underline;
    text-decoration-color: ${theme.colors.primary};
  }
`)

// prettier-ignore
const highlightedProjectsSection = (css`
  ${sectionStyles}

  h2 {
    a {
      font-size: 1rem;
      font-weight: 400;
    }
  }
`)

// prettier-ignore
const technologiesSection = (css`
  ${sectionStyles}

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  .technologies {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    margin: 0 -1rem;

    .technology {
      width: 100%;
      flex: 1 1 calc(100% / 3);
      max-width: calc(100% / 3);
      margin-bottom: 1.5rem;
      padding: 0 1rem;
      filter: drop-shadow(0 4px 8px rgba(0,0,0,0.2));
    }

    .react-logo {
      order: 0;
    }

    .gatsby-logo {
      order: 1;
    }

    .graphql-logo {
      order: 2;
    }

    .node-logo {
      order: 3;
    }

    .sass-logo {
      order: 4;
    }

    ${mq[1]} {
      display: grid;
      grid-template-rows: repeat(5, 80px);
      grid-template-columns: repeat(5, 160px);
      grid-gap: 0;
      justify-items: center;
      align-items: center;
      margin: 0;

      .technology {
        padding: 0;
        margin-bottom: 0;
        max-width: unset;
      }

      .react-logo {
        grid-area: 1 / 1 / 4 / 2;
      }

      .node-logo {
        grid-area: 3 / 2 / 6 / 3;
      }

      .gatsby-logo {
        grid-area: 1 / 3 / 4 / 4;
      }

      .sass-logo {
        grid-area: 3 / 4 / 6 / 5;
      }

      .graphql-logo {
        grid-area: 1 / 5 / 4 / 6;
      }
    }
  }
`)

// prettier-ignore
const contactSection = (css`
  ${sectionStyles}

  display: flex;
  align-items: center;
  justify-content: center;

  div {
    width: 80ch;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    background-color: #fff;
    padding: 2rem;
    border-radius: 0.5rem;
    text-align: center;
    box-shadow: 0 4px 50px  rgba(0,0,0,0.2);

    h2 {
      margin-bottom: 0rem;
    }

    a {
      font-size: 1.5rem;
    }
  }
`)

export {
  introSection,
  aboutMeSection,
  highlightedProjectsSection,
  technologiesSection,
  contactSection,
}
