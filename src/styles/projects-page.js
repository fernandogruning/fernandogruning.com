import { css } from "@emotion/core"

import { sectionStyles } from "./global-styles"

// prettier-ignore
const highlightedProjectsSection = (css`
  ${sectionStyles}
`)

export { highlightedProjectsSection }
