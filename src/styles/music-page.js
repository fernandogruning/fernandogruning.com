import { css } from "@emotion/core"

import { sectionStyles } from "./global-styles"
import mq from "../utils/media-queries"

// prettier-ignore
const bestArtistsSection = (css`
  ${sectionStyles}

  .row-item {
    display: flex;
    align-items: center;
    margin-bottom: 1rem;
  }
`)

const artistsRowItemStyles = ({ isTop, isTop3 }) => {
  // prettier-ignore
  const topBaseStyles = (css`
    flex: 1 1 100%;
    max-width: 100%;
  `)

  if (isTop && isTop3) {
    // prettier-ignore
    return (css`
      ${topBaseStyles}

      ${mq[0]} {
        flex: 1 1 100%;
        max-width: 100%;
      }

      ${mq[2]} {
        flex: 1 1 calc(100% / 3);
        max-width: calc(100% / 3);
      }
    `)
  }

  if (isTop) {
    // prettier-ignore
    return (css`
      ${topBaseStyles}

      ${mq[0]} {
        flex: 1 1 50%;
        max-width: 50%;
      }

      ${mq[1]} {
        flex: 1 1 calc(100% / 3);
        max-width: calc(100% / 3);
      }

      ${mq[2]} {
        flex: 1 1 25%;
        max-width: 25%;
      }
    `)
  }

  // prettier-ignore
  return (css`
    flex: 1 1 50%;
    max-width: 50%;

    ${mq[0]} {
      flex: 1 1 50%;
      max-width: 50%;
    }

    ${mq[1]} {
      flex: 1 1 20%;
      max-width: 20%;
    }

    ${mq[2]} {
      flex: 1 1 calc(100% / 6);
      max-width: calc(100% / 6);
    }
  `)
}

// prettier-ignore
const bestAlbumsSection = (css`
  ${sectionStyles}

  .row-item {
    flex: 1 1 50%;
    max-width: 50%;
    margin-bottom: 3rem;

    ${mq[1]} {
      flex: 1 1 calc(100% / 3);
      max-width: calc(100% / 3);
    }

    ${mq[2]} {
      flex: 1 1 calc(100% / 6);
      max-width: calc(100% / 6);
    }
  }
`)

// prettier-ignore
const recentlyPlayedSection = (css`
  ${sectionStyles}

  .row-item {
    flex: 1 1 50%;
    max-width: 50%;
    margin-bottom: 3rem;

    ${mq[1]} {
      flex: 1 1 calc(100% / 3);
      max-width: calc(100% / 3);
    }

    ${mq[2]} {
      flex: 1 1 20%;
      max-width: 20%;
      margin-bottom: 0;
    }
  }
`)

export {
  bestArtistsSection,
  artistsRowItemStyles,
  bestAlbumsSection,
  recentlyPlayedSection,
}
