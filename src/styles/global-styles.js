import { css } from "@emotion/core"

// prettier-ignore
const sectionStyles = (css`
  margin-bottom: 6rem;

  h2 {
    margin-bottom: 2rem;
  }
`)

// prettier-ignore
const introSection = (css`
  ${sectionStyles}
  padding-top: 6rem;

  h1 {
    margin-bottom: 0rem;
  }

  h3 {
    font-weight: 400;
  }
`)

export { sectionStyles, introSection }
