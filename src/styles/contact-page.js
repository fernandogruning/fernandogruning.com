import { css } from "@emotion/core"
import {
  sectionStyles as ogSectionStyles,
  introSection as ogIntroSection,
} from "./global-styles"
import mq from "../utils/media-queries"

// prettier-ignore
export const wrapperStyles = (css`
  display: flex;
  flex-wrap: wrap;
  margin-left: -1.25rem;
  margin-right: -1.25rem;
`)

// prettier-ignore
const sectionStyles = (css`
  ${ogSectionStyles}
  padding-left: 1.25rem;
  padding-right: 1.25rem;
  flex: 0 0 100%;
  max-width: 100%;
`)

// prettier-ignore
export const introSection = (css`
  ${ogIntroSection}
  ${sectionStyles}
`)

// prettier-ignore
export const contactLinkStyles = theme => (css`
  position: relative;
  display: block;
  align-items: center;
  font-size: 18px;
  margin-bottom: 1rem;
  color: inherit;
  transition: ${theme.styles.transition};

  .icon {
    position: relative;
    top: calc(((18px *1.2) - 18px) /2);
    display: inline-block;
    height: 18px;
    margin-right: 0.125rem;
    fill: currentColor;
    transition: ${theme.styles.transition};
  }

  &:hover {
    color: ${theme.colors.primaryDarker};

    .icon {
      fill: ${theme.colors.primaryDarker}
    }
  }
`)

// prettier-ignore
export const contactLinksSection = theme => (css`
  ${sectionStyles}

  ${mq[1]} {
    order: 1;
    flex: 1 1 40%;
    max-width: 40%;
  }

  ${mq[2]} {
    flex: 1 1 50%;
    max-width: 50%;
  }

  h3 {
    span {
      text-decoration: underline;
      text-decoration-color: ${theme.colors.primary};
    }
  }
`)

// prettier-ignore
export const contactFormSection = (css`
  ${sectionStyles}


  ${mq[1]} {
    flex: 1 1 60%;
    max-width: 60%;
  }

  ${mq[2]} {
    flex: 1 1 50%;
    max-width: 50%;
  }
`)

// prettier-ignore
export const contactFormStyles = theme => (css`
  .row-item {
    margin-bottom: 1.5rem;
  }

  .half-width {
    flex: 1 1 50%;
    max-width: 50%;
    padding: 0 0.5rem;
  }

  .full-width {
    flex: 0 0 100%;
    max-width: 100%;
  }

  .alert {
    margin-left: 1rem;
    padding: 0.5rem 1rem;
    border-radius: 0.5rem;

    &.alert-success {
      background-color: rgba(161, 218, 158, 0.3);
      color: rgb(44, 167, 44);
    }

    &.alert-failure {
      background-color: rgba(218, 158, 158, 0.3);
      color: ${theme.colors.primary};
    }
  }
`)

// prettier-ignore
export const buttonStyles = theme => (css`
  background-color: ${theme.colors.primary};
  padding: 0.5rem 1.5rem;
  border: 1px solid ${theme.colors.primary};
  border-radius: 0.25rem;
  font-size: 18px;
  color: #ffffff;
  line-height: 1;
  cursor: pointer;
  outline: none;
  transition: ${theme.styles.transition};

  &:hover {
    background-color: ${theme.colors.primaryDarker};
    border-color: ${theme.colors.primaryDarker};
  }

  &:disabled {
    background-color: #4b4241;
    border-color: #4b4241;
    cursor: not-allowed;
  }
`)
