import { css } from "@emotion/core"

import { sectionStyles } from "./global-styles"
import mq from "../utils/media-queries"

// prettier-ignore
const rowStyles = (css`
  .row-item {
    flex: 1 1 50%;
    max-width: 50%;
    margin-bottom: 3rem;

    ${mq[1]} {
      flex: 1 1 calc(100% / 3);
      max-width: calc(100% / 3);
    }

    ${mq[2]} {
      flex: 1 1 20%;
      max-width: 20%;
    }
  }
`)

// prettier-ignore
const favoriteFilmsSection = (css`
  ${sectionStyles}
  ${rowStyles}
`)

// prettier-ignore
const favoriteTVShowsSection = (css`
  ${sectionStyles}
  ${rowStyles}
  margin-bottom: 3rem;
`)

export { favoriteFilmsSection, favoriteTVShowsSection }
