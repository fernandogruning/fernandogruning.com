import { useStaticQuery, graphql } from "gatsby"

const useTopProjectsData = () => {
  const { allProjectsJson } = useStaticQuery(
    graphql`
      query {
        allProjectsJson(limit: 3) {
          nodes {
            id
            title
            githubURL
            gitlabURL
            image {
              childImageSharp {
                fluid(maxWidth: 576) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
            websiteURL
            description
          }
        }
      }
    `
  )

  return { projects: allProjectsJson.nodes }
}

export default useTopProjectsData
