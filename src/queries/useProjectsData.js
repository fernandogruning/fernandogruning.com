import { useStaticQuery, graphql } from "gatsby"

const useProjectsData = () => {
  const { allProjectsJson } = useStaticQuery(
    graphql`
      query {
        allProjectsJson {
          nodes {
            id
            title
            githubURL
            gitlabURL
            websiteURL
            description
            image {
              childImageSharp {
                fluid(maxWidth: 576) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    `
  )

  return { projects: allProjectsJson.nodes }
}

export default useProjectsData
