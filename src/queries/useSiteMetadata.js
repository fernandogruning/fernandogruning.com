import { useStaticQuery, graphql } from "gatsby"

const useSiteMetadata = () => {
  const { site } = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
          description
          author
          siteUrl
          lang
          projectsPage {
            title
            description
            url
          }
          musicPage {
            title
            description
            url
          }
          filmsTvPage {
            title
            description
            url
          }
          contactPage {
            title
            description
            url
          }
        }
      }
    }
  `)

  return site.siteMetadata
}

export default useSiteMetadata
