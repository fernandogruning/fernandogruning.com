const breakpoints = [480, 768, 992, 1200, 1344]

export default breakpoints.map(bp => `@media (min-width: ${bp}px)`)
