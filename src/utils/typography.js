import Typography from "typography"

import mq from "./media-queries"

const typography = new Typography({
  baseFontSize: "16px",
  baseLineHeight: "1.625",
  scaleRatio: 0,
  headerFontFamily: [
    "CircularStd",
    "-apple-system",
    "BlinkMacSystemFont",
    "Segoe UI",
    "Roboto",
    "Helvetica Neue",
    "Arial",
    "Noto Sans",
    "sans-serif",
    "Apple Color Emoji",
    "Segoe UI Emoji",
    "Segoe UI Symbol",
    "Noto Color Emoji",
  ],
  bodyFontFamily: [
    "Source Sans Pro",
    "-apple-system",
    "BlinkMacSystemFont",
    "Segoe UI",
    "Roboto",
    "Helvetica Neue",
    "Arial",
    "Noto Sans",
    "sans-serif",
    "Apple Color Emoji",
    "Segoe UI Emoji",
    "Segoe UI Symbol",
    "Noto Color Emoji",
  ],
  overrideStyles: ({ adjustFontSizeTo }) => ({
    body: {
      color: `hsla(0, 20%, 10%, 0.8)`,
    },
    img: {
      marginBottom: 0,
    },
    "h1, h2, h3, h4, h5, h6": {
      lineHeight: 1.1,
    },
    h1: {
      fontSize: "3rem",
    },
    h2: {
      fontSize: "2rem",
    },
    h3: {
      fontSize: "1.5rem",
    },
    h4: {
      fontSize: "1.25rem",
    },
    h5: {
      fontSize: "1.125rem",
    },
    h6: {
      fontSize: "1rem",
    },
    [mq[2]]: {
      h1: {
        fontSize: "4rem",
      },
      h2: {
        fontSize: "2.25rem",
      },
      h3: {
        fontSize: "1.75rem",
      },
    },
  }),
})

export default typography
