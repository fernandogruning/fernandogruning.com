require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})
const proxy = require("http-proxy-middleware")

module.exports = {
  siteMetadata: {
    title: "Fernando Gruning — Front-End Web Developer",
    description:
      "Hi! I'm Fernando Gruning, a Front-End Web Developer from Santo Domingo. In this website you can find my latest projects and my favorite music, films & TV shows.",
    siteUrl: "https://www.fernandogruning.com",
    lang: "en",
    author: "@fernandogruning",
    projectsPage: {
      title: "Projects | Fernando Gruning — Front-End Web Developer",
      description: "Showcasing my latest projects",
      url: "https://www.fernandogruning.com/projects",
    },
    musicPage: {
      title: "⭐️Music | Fernando Gruning — Front-End Web Developer",
      description: "My favorite artists, albums + my recently played tracks",
      url: "https://www.fernandogruning.com/music",
    },
    filmsTvPage: {
      title: "⭐️Films + TV | Fernando Gruning — Front-End Web Developer",
      description: "The films & TV shows that made me wonder",
      url: "https://www.fernandogruning.com/tv-films",
    },
    contactPage: {
      title: "Contact Me | Fernando Gruning — Front-End Web Developer",
      description: "Let's talk!",
      url: "https://www.fernandogruning.com/contact",
    },
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "Fernando Gruning — Front-End Web Developer",
        short_name: "Fernando Gruning",
        start_url: "/",
        background_color: "tomato",
        theme_color: "tomato",
        display: "minimal-ui",
        icon: `${__dirname}/src/images/favicon.png`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `project-images`,
        path: `${__dirname}/src/images/projects`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `artist-images`,
        path: `${__dirname}/src/images/artists`,
      },
    },
    `gatsby-transformer-json`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `projects`,
        path: `${__dirname}/src/content/projects.json`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `bestArtists`,
        path: `${__dirname}/src/content/best-artists.json`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-emotion`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
        omitGoogleFont: true,
      },
    },
    {
      resolve: `gatsby-source-discogs`,
      options: {
        userToken: process.env.DISCOGS_USER_ACCESS_TOKEN,
      },
    },
    {
      resolve: `gatsby-plugin-remote-images`,
      options: {
        nodeType: "DiscogsItem",
        imagePath: "image_url",
        name: "image",
      },
    },
    `gatsby-plugin-netlify`,
    {
      resolve: `gatsby-source-tmdb`,
      options: {
        apiKey: process.env.TMDB_API_KEY,
        sessionID: process.env.TMDB_SESSION_ID,
        modules: {
          account: {
            activate: true,
            endpoints: {
              list: "accountLists",
            },
          },
        },
      },
    },
  ],
  developMiddleware: app => {
    app.use(
      "/.netlify/functions",
      proxy({
        target: "http://localhost:9000",
        pathRewrite: { "/.netlify/functions/": "" },
      })
    )
  },
}
