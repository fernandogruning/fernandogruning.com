const getPopulatedDiscogsList = require("./lib/getPopulatedDiscogsList")

exports.createSchemaCustomization = ({ actions: { createTypes }, schema }) => {
  const typeDefs = `
  type DiscogsItem implements Node {
    id: String!
    uri: String!
    type: String!
    resource_url: String!
    image_url: String!
    display_title: String!
    comment: String!
    image_url: String!

  }

  type DiscogsList implements Node {
    id: String!
    name: String!
    image_url: String!
    description: String!
    date_changed: Date!
    date_added: Date!
    public: Boolean!
    uri: String!
    resource_url: String!
    items: [DiscogsItem]!
  }
`
  createTypes(typeDefs)
}

exports.sourceNodes = async (
  { actions: { createNode }, createNodeId, createContentDigest },
  { plugins, ...configOptions }
) => {
  const { items, ...list } = await getPopulatedDiscogsList(
    configOptions.userToken
  )
  const listId = createNodeId(`discogs-list-${list.id}`)
  createNode({
    ...list,
    id: listId,
    parent: null,
    children: [],
    internal: {
      type: "DiscogsList",
      content: JSON.stringify(list),
      contentDigest: createContentDigest(list),
    },
  })

  items.forEach(item => {
    createNode({
      ...item,
      id: createNodeId(`discogs-item-${item.id}`),
      parent: null,
      children: [],
      internal: {
        type: "DiscogsItem",
        content: JSON.stringify(item),
        contentDigest: createContentDigest(item),
      },
    })
  })
}

exports.createResolvers = ({
  actions: { createNode },
  cache,
  createNodeId,
  createResolvers,
  store,
  reporter,
}) => {
  createResolvers({
    DiscogsList: {
      items: {
        resolve(source, args, context, info) {
          return context.nodeModel.getAllNodes({ type: "DiscogsItem" })
        },
      },
    },
  })
}
