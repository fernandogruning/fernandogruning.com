const axios = require("axios")

const getDiscogsReleases = async (discogsItems, userToken) => {
  const fullItems = discogsItems.map(async discogsItem => {
    const response = await axios(
      `https://api.discogs.com/masters/${discogsItem.id}`,
      {
        headers: {
          "User-Agent": "FernandoGruning/2.0",
          Authorization: `Discogs token=${userToken}`,
        },
      }
    )
    const fullDiscogItem = await response.data
    return fullDiscogItem
  })
  return Promise.all(fullItems)
}

module.exports = getDiscogsReleases
