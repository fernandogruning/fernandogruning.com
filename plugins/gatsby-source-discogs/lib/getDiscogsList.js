const axios = require("axios")

// Simple function for fetching a list from the Discogs API
const getDiscogsList = async userToken => {
  try {
    const response = await axios("https://api.discogs.com/lists/556191", {
      headers: {
        "User-Agent": "FernandoGruning/2.0",
        Authorization: `Discogs token=${userToken}`,
      },
    })
    const data = await response.data
    return await response.data
  } catch (error) {
    console.log(error)
    throw new Error(error.message)
  }
}

module.exports = getDiscogsList
