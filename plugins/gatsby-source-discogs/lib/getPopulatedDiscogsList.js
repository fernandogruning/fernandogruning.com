const getDiscogsList = require("./getDiscogsList")
const getDiscogsReleases = require("./getDiscogsReleases")

// it's lit!
const fixTravisScottName = travisScottName =>
  travisScottName.replace(" (2)", "")

const getFixedItems = discogsItems =>
  discogsItems.map(item => {
    if (item.artist.toLowerCase().includes("travis scott")) {
      return { ...item, artist: fixTravisScottName(item.artist) }
    }
    return item
  })

const getPopulatedDiscogsList = async userToken => {
  const dicogsList = await getDiscogsList(userToken)
  const discogsReleases = await getDiscogsReleases(dicogsList.items, userToken)

  const discogsItems = dicogsList.items.map(
    ({ display_title, ...discogItem }) => {
      const discogRelease = discogsReleases.find(
        release => release.id === discogItem.id
      )

      return {
        ...discogItem,
        artist: discogRelease.artists[0].name,
        title: discogRelease.title,
        image_url: discogRelease.images[0].resource_url,
      }
    }
  )

  return { ...dicogsList, items: getFixedItems(discogsItems) }
}

module.exports = getPopulatedDiscogsList
